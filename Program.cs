﻿using System;
using System.Collections.Generic;

namespace animaleriePetshop
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Bienvenue ChezPetshop!");
            Animal chien1 = new Chien();
            Animal chat1 = new Chat();
            Animal chien2 = new Chien();
            Animal chien3 = new Chien();
            Animal chat2 = new Chat();
            var l = new List<Animal>{chien2, chien3, chat2};
            Magasin petshop = new Magasin();
            petshop.toString();
            petshop.acheterNAnimaux(l);
            petshop.toString();
            
            petshop.vendreUnAnimal(chien2);
            petshop.toString();

            petshop.vendreUnAnimal(chat2);
            petshop.toString();
        }
    }
}
