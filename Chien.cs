using System;
namespace animaleriePetshop
{
    class Chien : Animal
    {
        public double prix;

        public Chien(){
            this.prix = 100.00;
        }

        public override double getPrix(){
            return this.prix;
        }

        public override string getType(){
            return "chien";
        }

        public override void toString(){
            Console.WriteLine($"Type chien et prix {this.prix} !");
        }
    }
}