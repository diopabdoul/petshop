using System;

namespace animaleriePetshop
{
    public abstract class Animal
    {
        public abstract void toString();
        public abstract string getType();
        public abstract double getPrix();
    }
}