using System;
namespace animaleriePetshop
{
    class Chat : Animal
    {
        public double prix;

        public Chat(){
            this.prix = 110.00;
        }

        public override double getPrix(){
            return this.prix;
        }

        public override string getType(){
            return "chat";
        }
        
        public override void toString(){
            Console.WriteLine($"Type chat et prix {this.prix} !");
        }

    }
}