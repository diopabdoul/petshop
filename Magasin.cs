using System;
using System.Collections.Generic;

namespace animaleriePetshop
{
    public class Magasin
    {
        private List<Animal> listeAnimaux = new List<Animal>();
        private double tresorerie;
        public Magasin(){
            this.tresorerie = 1000;
        }

        public void ajouterAnimal(Animal a){
            this.listeAnimaux.Add(a);
        }

        public void supprimerAnimal(Animal a){
            this.listeAnimaux.Remove(a);
        }
        //methode permettant d'acheter un animal
        public void acheterUnAnimal(Animal a){
                this.ajouterAnimal(a);
                this.tresorerie -= a.getPrix();
        }

        //methode permettant d'acheter plusieurs animaux
        public void acheterNAnimaux(List<Animal> list){
            foreach(var a in list){
                acheterUnAnimal(a);
            }
        }
        //methode permettant de vendre un animal
        public void vendreUnAnimal(Animal a){
            var index = this.listeAnimaux.IndexOf(a);
            if(index != -1){
                this.supprimerAnimal(a);
                this.tresorerie += a.getPrix();
            }else{
                Console.WriteLine($"On a plus l'animal que vous avez demandé");
            }
        }

        //methode permettant de vendre plusieurs animaux
        public void vendreNAnimaux(List<Animal> list){
            foreach(var a in list){
                vendreUnAnimal(a);
            }
        }


        public void toString(){
            Console.WriteLine($"La tresorie contient {this.tresorerie} Il y a dans ce magasin :");
            foreach(var a in this.listeAnimaux){
                a.toString();
            }
            
        }

    }
}